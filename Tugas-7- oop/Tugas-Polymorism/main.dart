import 'persegi.dart';
import "segitiga.dart";
import 'lingkaran.dart';


void main(List<String> args) {
  print("Persegi");
  persegi Persegi = persegi(4.0);
  print("luas : ${Persegi.convertLuas()}");
  print("Keliling : ${Persegi.convertKeliling()}");
  print("\n");

  print("Segitiga");
  var Segitiga = segitiga(4.0,2.0,5.0,2.0,6.0);
  print("luas : ${Segitiga.convertLuas()}");
  print("Keliling : ${Segitiga.convertKeliling()}");
  print("\n");

  print("lingkaran");
  var lingkaran = Lingkaran(4.0,5.0);
  print("luas : ${lingkaran.convertLuas()}");
  print("Keliling : ${lingkaran.convertKeliling()}");


}