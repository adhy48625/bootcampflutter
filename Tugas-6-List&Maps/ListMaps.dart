void main(List<String> args) {
  print(range(1,10));
  print(range(10,1));
  print(rangeWithStep(10,1,2));
  print(rangeWithStep(1,10,4));
  dataHandling(2);
  print(balikKata("kasur"));
  print(balikKata("SanberCode"));
}

//NO 1
range(startNum, finishNum){
 var randomdata = [1,3,5,20,4,2];
 if (startNum > finishNum) {
   
   randomdata.sort((b, a)=> a.compareTo(b));
   return randomdata;
 } else {
   randomdata.sort();
   return randomdata;
 } 
}

//NO 2

rangeWithStep(startNum, finishNum,step){
 var randomdata = [1,3,5,20,4,2];


 if (startNum > finishNum) {  
  randomdata.sort((b, a)=> a.compareTo(b));
  print("sebelum : $randomdata");
    var randomdataStep = randomdata.where((element) => element %step != 0);
    return randomdataStep ;

 } else {
   randomdata.sort();
   print("sebelum : $randomdata");
   var randomdataStep = randomdata.where((element) => element %step != 0);
   return randomdataStep;
 } 
}

//NO 3
dataHandling(n){
  print("=================NO 3=================");
  var input = [
    ["0001","Roman Alamsyah", "bandar lampung", "21/05/1989","membaca"],
    ["0002","Dika Sembiring ", " Mendan", "10/10/1992","bermain gitar"],
    ["0003","Winona ", "ambon ", "25/12/1989","memasak"],
    ["0004","Bintang Senjaya", "Martapura", "6/4/1989","berkebun"]
    ];

  for (var i = 0; i <= n; i++) {
    print("Nomor :${input[i][0]}");
    print("Nama :${input[i][1]}");
    print("TTL :${input[i][2]} ${input[i][3]}");
    print("Hobi :${input[i][4]}");
    print("\n");
    }
}

//NO 4
balikKata(String kata){
  var input = kata; 
  return input.split('').reversed.join('');

  }