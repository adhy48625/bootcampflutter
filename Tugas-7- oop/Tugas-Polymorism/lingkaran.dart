import 'bangun_datar.dart';
class Lingkaran extends bangun_datar {

  double jari =0;
  double pi =0;
  
  Lingkaran(jari,pi){
  this.jari =jari;
  this.pi =pi;
  }

  @override
  double convertLuas() {
    return  3.14 * jari * jari;
  }

  @override
  double convertKeliling() {
    // TODO: implement convertKeliling
    return  2 * pi * jari ;
  }


   
}