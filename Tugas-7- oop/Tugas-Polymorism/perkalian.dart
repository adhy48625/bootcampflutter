import 'bangun_datar.dart';
class Perkalian extends bangun_datar {
  double a=0;
  double b=0;
  Perkalian(double a, double b){
    this.a =a;
    this.b = b;
  }

  @override
  double convert() {
    return a * b ;
  }
}