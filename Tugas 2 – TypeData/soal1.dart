import 'dart:io';
void main(List<String> args) {
  //SOAL 1
  print("=======================Soal 1=======================");
  var word = 'dart';
  var second = 'is';
  var third = 'awesome';
  var fourth = 'and';
  var fifth = 'I';
  var sixth = 'love';
  var seventh = 'it!';
  print('$word '+"$second "+"$third "+"$fourth "+"$fifth "+"$sixth "+"$seventh ");
  
  //SOAL 2
    print("=======================Soal 2=======================");

  var sentence = "I am going to be Flutter Developer";
  var exampleFirstWord = sentence[0] ;
  var secondWord = sentence[2] + sentence[3] ;
  var thirdWord = "going"; // lakukan sendiri
  var fourthWord = "to"; // lakukan sendiri
  var fifthWord ="be"; // lakukan sendiri
  var sixthWord ="Flutter"; // lakukan sendiri
  var seventhWord = "Developer"; // lakukan sendiri


  print('First Word: ' + exampleFirstWord);
  print('Second Word: ' + secondWord);
  print('Third Word: ' + thirdWord);
  print('Fourth Word: ' + fourthWord);
  print('Fifth Word: ' + fifthWord);
  print('Sixth Word: ' + sixthWord);
  print('Seventh Word: ' + seventhWord);

//SOAL 3
  print("=======================Soal 3=======================");
  print("masukan nama depan");
  String inputText = stdin.readLineSync()!;
  print("nama depan : ${inputText}");
  print("masukan nama belakang");
  String inputText2 = stdin.readLineSync()!;
  print("nama belakang : ${inputText2}");
  print("nama lengkap anda adalah: $inputText $inputText2");

 //SOAL 4
  print("=======================Soal 4=======================");
    int a = 5;
    int b = 10;
    print("perkalian : ${a*b}");
    print("pembagian : ${a/b}");
    print("penambahan : ${a+b}");
    print("pengurangan : ${a-b}");

 

}


