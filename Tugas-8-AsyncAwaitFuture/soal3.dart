

Future<String> line1() =>
    Future.delayed(
      Duration(seconds: 5),
      () => 'pernahkah kau merasa',
    );

Future<String> line2() =>
    Future.delayed(
      Duration(seconds: 3),
      () => 'pernahkah kau merasa...',
    );

Future<String> line3() =>
    Future.delayed(
      Duration(seconds: 2),
      () => 'pernahkah kau merasa',
    );
  
Future<String> line4() =>
    Future.delayed(
      Duration(seconds: 1),
      () => 'Hatimu hampa, pernahkah kau merasa hatimu kosong....',
    );


// jdi awal masuk akan mencetak "Ready. sing" setelah itu ada jeda 5 detik kemudian akan mucul line ( ) yang akan mencetak pernahkan kau merasa,

// setelah mencetak line() kemudian ada jeda waktu 3 detik untuk mencetak line2( ) yang berisi pernahkah kau merasa ........... 

// setelah mencetak line2() kemudian ada jeda waktu 2 detik untuk mencetak line3( ) yang berisi pernahkah kau merasa,

// dan terakhir yaitu memunculkan  line4( ) dengan jeda 1 detik dari line 3 ke line 4yang berisi,  hatimu hampa pernahkah kau merasa hati mu kosong ............


Future<void> main() async {
 print('Ready. sing');
 print(await line1());
 print(await line2());
 print(await line3());
 print(await line4());
}


