import 'package:flutter/material.dart';
import 'package:sanberappflutter/Quiz3/login_screen.dart';
import 'package:sanberappflutter/Quiz3/model.dart';

class Homescreen extends StatefulWidget {
  
  Homescreen({
    Key? key,
   
  }) : super(key: key);

  @override
  _HomescreenState createState() => _HomescreenState();
}


class _HomescreenState extends State<Homescreen> {

  _HomescreenState();
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        
        padding: EdgeInsets.all(16),
        margin: EdgeInsets.only(top: 20),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(40.0),
                      child: Image.network(
                        "https://avatars.githubusercontent.com/u/52710807?v=4",
                        height: 80,
                        width: 80,
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    
                    //Text(value.toString()),
                  ],
                ),
                Row(
                  children:[
                    //## soal 4
                    //Tulis Coding disini
                    
                    Text("Rp "),
                    
                    //sampai disini
                    SizedBox(
                      width: 10,
                    ),
                    Icon(Icons.add_shopping_cart)
                  ],
                ),
              ],
            ),
            SizedBox(height : MediaQuery.of(context).size.height / 1.5,
            //#soal 3 silahkan buat coding disini
            //untuk container boleh di pake/dimodifikasi 
            
              child: GridView.count(
                padding: EdgeInsets.all(9.0),
                crossAxisCount: 2,
                childAspectRatio: 3/2,
                crossAxisSpacing: 10,
                
                children: List.generate(8, (index) {

            return ListTile(
              
                  leading: Image.network(items[index].profileUrl),
                  title: Text (items[index].name),                  
                  subtitle: Text("RP:${items[index].price.toString()}"),
                  trailing: Container(  
                  height : 100 ,         
                  padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                  child: ElevatedButton(              
                    style: raisedButtonStyle,
                    child: Text("Beli"),
                    onPressed: () {
                               
                    }
                ),  
)
                
              );
              
              
              
            }
            ),
                
                
              )),
            Container(
              height : 100 ,       
              //child: 
            ),
  

            //sampai disini soal 3
           
          ],
        ),
      ),
    );
  }
}

Widget myWidget(BuildContext context) {
  return MediaQuery.removePadding(
    context: context,
    removeTop: true,
    child: GridView.builder(
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
        ),
        itemCount: 300,
        itemBuilder: (BuildContext context, int index) {
          return Card(
            color: Colors.amber,
            child: Center(child: Text('$index')),
          );
        }),
  );
}

final ButtonStyle raisedButtonStyle = ElevatedButton.styleFrom(
  onPrimary: Colors.grey[300],
  primary: Colors.blue[300],
  minimumSize: Size(88, 36),
  padding: EdgeInsets.symmetric(horizontal: 16),
  shape: const RoundedRectangleBorder(
    borderRadius: BorderRadius.all(Radius.circular(6)),
  ),
);



