import 'dart:io';
void main(List<String> args) {

  //NO 1 
  print("============NO1============");
  teriak() => print("teriak");
  teriak();

  //NO2
  print("============NO2============");
  adhy(num1, num2) => num1 * num2 ; 
  var hasilKali = adhy(4,12);
  print(hasilKali);

  //no3
  print("============NO3============");
  introduce(name, age, address, hobby) => print("nama saya $name, umur saya $age, alamat saya $address, hobby saya $hobby");
  introduce("adhy","22","jakarta","bermain game");

  //no4
  print("============NO4============");
  faktorial(int angka){
    if (angka <= 0) {
    return 1;
  }
    return angka * faktorial(angka - 1);
  }
  
//cetak hasil function faktorial 
  int angka = 6;
  var cetak_faktorial = faktorial(angka);
  print("$angka! = $cetak_faktorial");

//Cetak penejelasan faktorial
 stdout.write("didapat dari $angka! = ");
 for (var i = 1; angka >= i; angka--) {
   stdout.write("$angka");
   if (angka != i ) {
     stdout.write(" * ");
   }
 }
}