import 'dart:io';
void main(){
  //1. Ternary operator 
      print("==============1. Ternary operator ==============");
      print("ketik y untuk instal, t untuk menolak");
      String? input = stdin.readLineSync();   
      input?.toLowerCase() == "y"? print("anda akan menginstall aplikasi dart") : input?.toLowerCase() == "t"  ? print("aborted"):  print("tidak ada pilihan");

   // 2. If-else if dan else
    print("==============2. If-else if dan else ==============");
    print("Nama harus diisi!");
    String? nama = stdin.readLineSync(); 
    print("Pilih Peranmu untuk memulai game");
    String? peran = stdin.readLineSync(); 
    
    if (peran !=  "guard" && peran !=  "warewolf" && peran !=  "penyihir" ) {
      print("$nama piih peran anda");
    } else if(peran == "guard")  {
      print("Selamat datang di Dunia Werewolf, $nama Halo $peran $nama, kamu akan membantu melindungi temanmu dari serangan werewolf");
    } else if(peran == "warewolf"){
      print("Selamat datang di Dunia Werewolf, $nama Halo $peran $nama, Kamu akan memakan mangsa setiap malam!");
    } else if(peran == "penyihir"){
      print("Selamat datang di Dunia Werewolf, $nama Halo $peran $nama, kamu dapat melihat siapa yang menjadi werewolf!");
    }else {
      print("masukkan nama dan peran");
    }

    // 3. Switch case
    print("==============3. Switch case==============");
    print("pilih hari :");
    String? hari = stdin.readLineSync(); 
    switch (hari?.toLowerCase()) {
      case "senin" : {print("Segala sesuatu memiliki kesudahan, yang sudah berakhir biarlah berlalu dan yakinlah semua akan baik-baik saja."); break;} 
      case "selasa" : {print("Setiap detik sangatlah berharga karena waktu mengetahui banyak hal, termasuk rahasia hat"); break;} 
      case "rabu" : {print("Jika kamu tak menemukan buku yang kamu cari di rak, maka tulislah sendiri."); break;}
      case "kamis" : {print("Jika hatimu banyak merasakan sakit, maka belajarlah dari rasa sakit itu untuk tidak memberikan rasa sakit pada orang lain."); break;}
      case "jumat" : {print("Hidup tak selamanya tentang pacar."); break;}
      case "sabtu" : {print("Rumah bukan hanya sebuah tempat, tetapi itu adalah perasaan."); break;}
      case "minggu" : {print("Hanya seseorang yang takut yang bisa bertindak berani. Tanpa rasa takut itu tidak ada apapun yang bisa disebut berani."); break;} 
      default: {print("jajaj");}
    }
    
// 4. Switch case
    print("==============4. Switch case==============");
    var tanggal = 10; 
    var bulan = 4; 
    var tahun = 1999;

    if(tanggal <=31 && bulan <= 12 && tahun <=2200 && tahun >=1900){
        switch(bulan) {
        case 1:   { print('$tanggal Januari $tahun'); break; }
        case 2:   { print('$tanggal Februari $tahun'); break; }
        case 3:   { print('$tanggal Maret $tahun'); break; }
        case 4:   { print('$tanggal April $tahun'); break; }
        case 5:   { print('$tanggal Mei $tahun'); break; }
        case 6:   { print('$tanggal Juni $tahun'); break; }
        case 7:   { print('$tanggal Juli $tahun'); break; }
        case 8:   { print('$tanggal Agustus $tahun'); break; }
        case 9:   { print('$tanggal September $tahun'); break; }
        case 10:   { print('$tanggal Oktober $tahun'); break; }
        case 11:   { print('$tanggal November $tahun'); break; }
        case 12:   { print('$tanggal Desember $tahun'); break; }
        default:  { print('Terjadi kesalahan pada tanggal'); }}
    }else{
      print("'Terjadi kesalahan pada tanggal'");
    }
}
    

    
    