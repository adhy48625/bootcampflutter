
void main(List<String> args) {
  // //No. 1 Looping While 
  print("=============1 While============= ");
  var loop = 1;
  while (loop <= 20) {
    if (loop == 1) {
      print ("LOOPING PERTAMA");
      loop++;
      
    }   
    print("${loop++}- I love coding");  
  }
  while (loop >= 2) {
    if (loop == 21) {
      print ("LOOPING Kedua");
      loop--;
    } 
    
    print("${loop--}- I will become a mobile developer");  
  }

  //No. 2 FOR
  print("=============2 FOR============= ");
  for(int i=1; i<=20;i++){
    if(i % 2 == 1 && i % 3 == 0){
      print("$i - I Love Coding ");
    }
    else if(i % 2 == 1  ){
      print("$i - santai ");
    }
    else {
      print("$i - Berkualitas");
    }
  }

  //No. 3 Persegi Panjang

  print("=============3 Persegi Panjang============= ");
  for ( int i=0 ; i <= 4; i++) {
    print("#" * 8); 
    
  }

//No. 4 Tangga 
  print("=============4 Tangga ============= ");
  for(int i=0;i<=7;i++){
        print("*" * i);
     }  
}
  
  

  

  
  
